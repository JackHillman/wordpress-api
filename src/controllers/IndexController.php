<?php

namespace api\controllers;

use api\app\Post;
use api\base\BaseController;

class IndexController extends BaseController
{
    public function name()
    {
        return 'index';
    }

    public function transform(Post $post)
    {
        return [
            'title' => $post->getTitle(),
            'content' => $post->getContent(),
            'permalink' => $post->getPermalink(),
            'terms' => $post->getTerms(),
            'author' => $post->getAuthor(),
            'date' => $post->getDate(),
            'postType' => $post->postType,
        ];
    }
}
