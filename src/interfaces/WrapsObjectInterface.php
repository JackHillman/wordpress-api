<?php

namespace api\interfaces;

interface WrapsObjectInterface
{
    function wrappedObject();
}
