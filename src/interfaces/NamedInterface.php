<?php

namespace api\interfaces;

interface NamedInterface
{
    function name();
}
