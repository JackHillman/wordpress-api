<?php

namespace api\providers;

use api\app\Query;
use api\controllers\IndexController;
use api\providers\HookServiceProvider;
use Illuminate\Support\Arr;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class RouteServiceProvider extends HookServiceProvider
{
    protected $filters = [
        'template_include' => 'templateInclude',
        'wp' => 'maybeRemoveCannonical',
    ];

    public function maybeRemoveCannonical() {
        if ($this->isApiRoute()) {
            // WordPress will attempt to redirect "equivalent" links for SEO
            // We don't need this, and it causes issues when loading the homepage
            remove_action('template_redirect', 'redirect_canonical');
        }
    }

    public function isApiRoute()
    {
        $query = new Query;

        return (bool) Arr::get($query->query_vars, 'api', 0);
    }

    public function register()
    {
        collect([
            'index', '404', 'archive', 'author', 'category', 'tag',
            'taxonomy', 'date', 'home', 'frontpage', 'page', 'paged',
            'search', 'single', 'singular', 'attachment',
        ])->map(function ($type) {
            $this->filters["{$type}_template"] = 'addTemplate:' . PHP_INT_MAX;
        });
    }

    public function addTemplate($template, $type, $templates)
    {
        if (!$this->isApiRoute()) {
            return $template;
        }

        $controllerNamespaces = api('config')->get('api.controllers');

        $controllers = collect($templates)->map(function ($template) use ($controllerNamespaces) {
            $transforms = [
                '%^/?(resources[\\/]views)?[\\/]?%' => '',
                '%(\.blade)?(\.php)?$%' => '',
            ];
            $normalizedTemplate = preg_replace(array_keys($transforms), array_values($transforms), $template);
            $className = studly_case($normalizedTemplate);

            return collect($controllerNamespaces)->map(function ($namespace) use ($className) {
                return "{$namespace}\\{$className}Controller";
            });
        })->flatten()->filter(function ($class) {
            return class_exists($class);
        });

        if ($controllers->count() === 0) {
            $controllers = collect($controllerNamespaces)->map(function ($namespace) {
                return "{$namespace}\\IndexController";
            })->flatten()->filter(function ($class) {
                return class_exists($class);
            });
        }

        $controller = $controllers->count() === 0
            ? IndexController::class
            : $controllers->first();

        if (!$controller) {
            abort(404);
        }

        return $controller;
    }

    public function templateInclude($template)
    {
        if (class_exists($template)) {
            $controller = $this->app->make($template);
            $method = strtolower($this->app->request->getMethod());

            if (method_exists($controller, $method)) {
                $response = $this->callController($controller, $method);
            } else {
                // Abort with 405: Method not allowed
                abort(405);
            }

            if ($response instanceof SymfonyResponse) {
                $response->send();
            } else {
                echo (string) $response;
            }

            return API_DIR . '/index.php';
        }

        return $template;
    }

    public function callController($controller, $method)
    {
        try {
            return $this->prepareResponse(
                $this->call([$controller, $method])
            );
        } catch (HttpResponseException $e) {
            return $e->getResponse();
        }
    }

    public function prepareResponse($response)
    {
        return new JsonResponse($response);
    }

    public function call(callable $callback, array $params = [])
    {
        return call_user_func_array($callback, $params);
    }
}
