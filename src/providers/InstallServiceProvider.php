<?php

namespace api\providers;

use api\providers\HookServiceProvider;

class InstallServiceProvider extends HookServiceProvider
{
    protected $filters = [
        'query_vars' => 'addApiQueryVar',
        'rewrite_rules_array' => 'addApiRewriteRules:' . PHP_INT_MAX,
    ];

    public function addApiQueryVar($vars)
    {
        return array_merge($vars, ['api']);
    }

    public function addApiRewriteRules($rules)
    {
        if (get_option('permalink_structure')) {
            $newRules = [];

            $home = get_option('page_on_front');
            if ($home) {
                $newRules['^api/?$'] = "index.php?page_id={$home}&api=1";
            } else {
                $newRules['^api/?$'] = "index.php?api=1";
            }

            foreach ($rules as $key => $rule) {
                if (strpos($key, 'wp-json') === false) {
                    $newRules["api/{$key}"] = "$rule&api=1";
                }
            }

            $rules = array_merge($newRules, $rules);
        }

        return $rules;
    }
}
