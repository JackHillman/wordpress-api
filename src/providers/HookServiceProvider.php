<?php

namespace api\providers;

use api\exceptions\InvalidAbstractException;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Arr;

class HookServiceProvider extends ServiceProvider
{
    protected $actions = [];

    protected $filters = [];

    protected $abstractPattern = '/(?<method>[\w\/]+)(?::(?<priority>\d*))?/';

    public function boot()
    {
        foreach ($this->actions as $hook => $abstract) {
            $priority = $this->getPriority($abstract);
            $method = $this->getMethod($abstract);
            $arguments = $this->getArgumentCount($method);

            add_action($hook, [$this, $method], $priority, $arguments);
        }

        foreach ($this->filters as $hook => $abstract) {
            $priority = $this->getPriority($abstract);
            $method = $this->getMethod($abstract);
            $arguments = $this->getArgumentCount($method);

            add_filter($hook, [$this, $method], $priority, $arguments);
        }
    }

    private function getPriority($abstract)
    {
        $matches = [];

        if (!preg_match($this->abstractPattern, $abstract, $matches)) {
            throw new InvalidAbstractException($abstract);
        }

        return (int) Arr::get($matches, 'priority', 10);
    }

    private function getMethod($abstract)
    {
        $matches = [];

        if (!preg_match($this->abstractPattern, $abstract, $matches)) {
            throw new InvalidAbstractException($abstract);
        }

        return Arr::get($matches, 'method');
    }

    private function getArgumentCount($callback)
    {
        return (new \ReflectionMethod($this, $callback))->getNumberOfParameters();
    }
}
