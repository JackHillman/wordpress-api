<?php

// If this is installed with composer, the autoload won't be loaded here
$composer = API_DIR . '/../vendor/autoload.php';
if (file_exists($composer)) {
    require_once $composer;
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new api\Application();

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

if ($app->hasWordPress()) {
    $app->register(api\providers\InstallServiceProvider::class);
    $app->register(api\providers\RouteServiceProvider::class);
}

return $app;
