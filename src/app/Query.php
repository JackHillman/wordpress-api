<?php

namespace api\app;

use WP_Query;
use api\traits\WrapsObjectTrait;
use api\interfaces\WrapsObjectInterface;

class Query implements WrapsObjectInterface
{
    use WrapsObjectTrait;

    private $query;

    public function __construct()
    {
        global $wp_query;

        $this->query = $wp_query;
    }

    public function wrappedObject()
    {
        return $this->query;
    }

    public function isArchive()
    {
        return $this->query->is_archive() || $this->query->is_home();
    }
}
