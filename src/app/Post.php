<?php

namespace api\app;

use WP_Post;
use api\traits\WrapsObjectTrait;
use api\interfaces\WrapsObjectInterface;

class Post implements WrapsObjectInterface
{
    use WrapsObjectTrait;

    private $post;

    public function __construct($post = null)
    {
        $this->post = get_post($post);
    }

    public function wrappedObject()
    {
        return $this->post;
    }

    public function getTitle()
    {
        return get_the_title($this->post);
    }

    public function getContent()
    {
        return apply_filters('the_content', $this->post->post_content);
    }

    public function getPermalink()
    {
        return get_the_permalink($this->post);
    }

    public function getTerms()
    {
        $allTerms = [];
        $taxonomies = get_post_taxonomies($this->post);

        foreach ($taxonomies as $taxonomy) {
            $terms = get_the_terms($this->post, $taxonomy);
            if ($terms) {
                $allTerms[$taxonomy] = $terms;
            }
        }

        return $allTerms;
    }

    public function getAuthor()
    {
        return get_the_author_meta('display_name', $this->post->post_author);
    }

    public function getDate()
    {
        return get_the_date('U', $this->post);
    }
}
