<?php

namespace api;

use api\base\BaseContainer;
use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Container\Container as ContainerContract;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application as LumenApplication;

class Application extends BaseContainer
{
    protected $loadedProviders = [];

    protected $ranServiceBinders = [];

    protected $availableBindings = [
        'config' => 'registerConfigBindings',
    ];

    protected static $api;

    public function version()
    {
        return '0.0.1';
    }

    public function basePath()
    {
        return API_PATH;
    }

    public function __construct()
    {
        $this->bootstrapContainer();
        $this->registerContainerAliases();
    }

    public static function getInstance()
    {
        return self::$api ?: self::$api = new self;
    }

    public static function setInstance(ContainerContract $api = null)
    {
        self::$api = $api;
    }

    protected function bootstrapContainer()
    {
        self::setInstance($this);

        $this->instance('api', $this);

        $this->instance(BaseContainer::class, $this);
    }

    protected function registerContainerAliases()
    {
        $this->aliases = [
            'request' => 'Illuminate\Http\Request',
        ];
    }

    public function hasWordPress()
    {
        return defined('ABSPATH');
    }

    public function make($abstract, array $parameters = [])
    {
        $abstract = $this->getAlias($abstract);

        if (array_key_exists($abstract, $this->availableBindings) &&
            ! array_key_exists($this->availableBindings[$abstract], $this->ranServiceBinders)) {
            $this->{$method = $this->availableBindings[$abstract]}();

            $this->ranServiceBinders[$method] = true;
        }

        return parent::make($abstract, $parameters);
    }

    public function register($provider)
    {
        if (! $provider instanceof ServiceProvider) {
            $provider = new $provider($this);
        }

        if (array_key_exists($providerName = get_class($provider), $this->loadedProviders)) {
            return;
        }

        $this->loadedProviders[$providerName] = true;

        if (method_exists($provider, 'register')) {
            $provider->register();
        }

        if (method_exists($provider, 'boot')) {
            return $this->call([$provider, 'boot']);
        }
    }

    public function registerDeferredProvider($provider)
    {
        return $this->register($provider);
    }

    public function registerConfigBindings()
    {
        $defaults = $this->getConfigDefaults();

        $this->singleton('config', function () use ($defaults) {
            return new ConfigRepository($defaults);
        });
    }

    protected function getConfigDefaults()
    {
        return [
            'api' => [
                'response' => [
                    'withData' => true,
                    'withFrame' => true,
                    'withTemplate' => true,
                    'withType' => true,
                    'withPagination' => true,
                ],
                'controllers' => ['api\\controllers'],
            ],
        ];
    }
}
