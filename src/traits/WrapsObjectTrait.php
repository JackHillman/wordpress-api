<?php

namespace api\traits;

trait WrapsObjectTrait
{
    public function __get($name)
    {
        $snake = snake_case($name);
        if (property_exists($this->wrappedObject(), $snake)) {
            return $this->wrappedObject()->$snake;
        }

        return $this->wrappedObject()->$name;
    }

    public function __set($name, $value)
    {
        $snake = snake_case($name);
        if (property_exists($this->wrappedObject(), $snake)) {
            return $this->wrappedObject()->$snake = $value;
        }

        $this->wrappedObject()->$name = $value;
    }

    public function __call($name, $args)
    {
        $snake = snake_case($name);
        if (method_exists($this->wrappedObject(), $snake)) {
            return $this->wrappedObject()->$snake($args);
        }

        return $this->wrappedObject()->$name($args);
    }
}
