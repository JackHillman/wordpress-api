<?php

namespace api\base;

use api\app\Post;
use League\Fractal\TransformerAbstract;

class BaseTransformer extends TransformerAbstract
{
    public function transform(Post $post)
    {
        return $post->toArray();
    }
}
