<?php

namespace api\base;

use API;
use api\app\Post;
use api\app\Query;
use api\interfaces\NamedInterface;
use Illuminate\Support\Arr;

abstract class BaseController implements NamedInterface
{
    public function __construct(Query $query)
    {
        $this->query = $query;
    }

    public function get()
    {
        $response = collect();

        if (api('config')->get('api.response.withData')) {
            $response->put('data', $this->data());
        }

        if (api('config')->get('api.response.withFrame')) {
            $response->put('frame', $this->frame());
        }

        if (api('config')->get('api.response.withTemplate')) {
            $response->put('template', $this->name());
        }

        if (api('config')->get('api.response.withType')) {
            $response->put('type', $this->type());
        }

        if (api('config')->get('api.response.withPagination')) {
            $response->put('pagination', $this->pagination());
        }

        return $response;
    }

    public function frame()
    {
        return [
            'header' => $this->header(),
            'footer' => $this->footer(),
        ];
    }

    public function header()
    {
        return null;
    }

    public function footer()
    {
        return null;
    }

    public function transformer()
    {
        return method_exists($this, 'transform')
            ? [$this, 'transform']
            : new BaseTransformer();
    }

    public function data()
    {
        $posts = array_map(function ($post) {
            return new Post($post);
        }, $this->getPosts());

        $posts = fractal($posts, $this->transformer())->toArray()['data'];

        return $this->isArchive() ? $posts : $posts[0];
    }

    public function type()
    {
        return $this->isArchive() ? 'archive' : 'single';
    }

    public function pagination()
    {
        if (!$this->isArchive()) {
            return null;
        }

        $page = Arr::get($this->query->query, 'paged')
            ? (int) Arr::get($this->query->query, 'paged')
            : (int) Arr::get($this->query->query_vars, 'paged', 1);

        $links = [
            'previous' => $page > 1 ? previous_posts(false) : null,
            'next' => $page < $this->query->max_num_pages ? next_posts(0, false) : null,
        ];

        $total = (int) $this->query->found_posts;
        $current = $this->query->query_vars['posts_per_page'] * $page;

        return [
            'total' => $total,
            'current' => $current <= $total ? $current : $total,
            'links' => $links,
        ];
    }

    public function __call($name, $args)
    {
        return $this->query->$name($args);
    }
}
