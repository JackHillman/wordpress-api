<?php
/*
Plugin Name: API
Plugin URI: https://example.com/
Description: Adds custom API endpoint
Version: 0.0.1
Author: Jack Hillman
Author URI: https://jackhillman.com.au
Text Domain: api
*/

use api\Application;

defined('ABSPATH') or die();

define('API_DIR', __DIR__ . '/src');

$app = require API_DIR . '/bootstrap/api.php';

Application::setInstance($app);

if (!function_exists('api')) {
    function api($make = null) {
        $api = Application::getInstance();

        return $make ? $api->make($make) : $api;
    }
}

register_deactivation_hook(__FILE__, 'flush_rewrite_rules');
register_activation_hook(__FILE__, 'flush_rewrite_rules');
